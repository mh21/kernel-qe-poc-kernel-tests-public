# Public kernel-tests repository for kernel QE open-by-default proof-of-concept

For the main branch, the updated code is synced to the internal mirror at
<https://gitlab.cee.redhat.com/api/v4/projects/cki-project%2fkernel-qe-poc%2fkernel-tests-mirror/repository/archive.zip?sha=refs/heads/public/main>.

A test commit of direct push on public repo of gitlab.com
